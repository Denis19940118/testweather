import React from 'react';
import { Redirect, Route, Switch } from 'react-router';
import Home from '../pages/Home';

const AppRouter = () => {
    return (
        <section>
            <Switch>
              <Redirect exact path="/" to="/home"/>
              <Route exact path="/home">
                  <Home />
              </Route>
            </Switch>
        </section>
    );
};

export default AppRouter;
