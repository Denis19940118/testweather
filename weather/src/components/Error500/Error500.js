import React, { PureComponent } from 'react';
import deathStar from '../../theme/death-star.png';
import './Error500.scss';

class Error500 extends PureComponent {
    render() {
        return (
            <div className="error-500">
                <div>
                    <img src={deathStar} alt="death star"></img>
                    <h3>An error has occurred</h3>
				    <h4>But we already sent droids to fix it</h4>
                </div>
            </div>
        )
    }
};

export default Error500;
