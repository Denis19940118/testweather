import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.scss';

const Button = ({onClick, className, type, title, disabled}) => {
    return (
        <button type={type} className={className} onClick={onClick} disabled={disabled}>
            {title}
            </button>

    )
}

Button.propTypes = {
    onClick: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
}
Button.defaultProps = {
	className: styles.btn,
};

export default Button;
