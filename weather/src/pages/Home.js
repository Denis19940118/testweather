import React, { useState } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios';
import Button from '../components/Button/Button';
import useGeoLocation from '../hooks/useGeoLocation/useGeoLocation';

const LINK = 'https://www.metaweather.com/api/location/search/?lattlong='

const Home = () => {
    const [dis, setDis] = useState(false);
    const [res, setRes] = useState([])
    let location = useGeoLocation();
    console.log("location", location)
    const lat = location.coordinates.lat
    const lng = location.coordinates.lng
    const lattlong = `${lat},${lng}`;

    const clic = () => {
    setDis(!dis);
    // fetch('https://www.metaweather.com/api/location/search/?lattlong=50.411239699999996,30.3540413',{
    //     method:'GET',
    //     mode: 'no-cors', 
    //     cache: 'no-cache',
    //     credentials: 'same-origin', 
    // })  
    // .then(  
    //   function(response) {  
    //     if (response.status !== 200) {  
    //       console.log('Looks like there was a problem. Status Code: ' +  
    //         response.status);  
    //       return;  
    //     }
  
    //     // Examine the text in the response  
    //     response.json().then(function(data) {  
    //       console.log(data);  
    //     });  
    //   }  
    // )  
    // .catch(function(err) {  
    //   console.log('Fetch Error :-S', err);  
    // });
    
   
    
    // fetch(`https://www.metaweather.com/api/location/search/?lattlong=50.411239699999996,30.3540413`, {
    //     method: 'GET', 
    //     mode: 'no-cors', 
    //     cache: 'no-cache',
    //     credentials: 'same-origin', 
    //     headers: {
    //       'Content-Type': 'application/json',
    //       'Access-Control-Allow-Origin':'http://localhost:3000',
    //     },
    // }).then((res)=> res.json().then(data =>console.log(data)));
    axios({
        method: 'get',
        url: `https://www.metaweather.com/api/location/search/?lattlong=50.411239699999996,30.3540413`,
        headers:{
            'Content-Type': 'application/json',
            "Access-Control-Allow-Origin":"http://localhost:3000",
            "Accept-Encoding" : "gzip, deflate, br",
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"
        },
        mode: 'no-cors', 
        cache: 'no-cache',
        credentials: 'same-origin',
        withCredentials: false,
    //     params: {
    //         lattlong
    //    }
    }).then((res)=> console.log(res))
    }
console.log(res)
    const canc = () => {

    }

    return (
        <div>
            <h4>"Accept your GPS data"</h4>
            <Button type='button' title='Ok' onClick={clic} disabled={dis}/> 
            <Button type='button' title='Cancel' onClick={canc}/> 
            <p>Your location : {location.loaded ? JSON.stringify(location) 
            : "Location data not available yet"}</p>
            {dis && <div>
                
            <p>
                Your weather: {location.loaded ? res 
            : "Location data not available yet"}
            </p>
            </div>}

        </div>
    )
}

Home.propTypes = {

}

export default Home;
